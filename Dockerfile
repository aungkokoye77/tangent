FROM php:7.3-apache

RUN apt-get update && apt-get install vim -y

RUN apt-get update -y && apt-get install -y libxml2-dev \
  && apt-get clean -y && docker-php-ext-install soap

RUN yes | pecl install xdebug \
    && echo "zend_extension=$(find /usr/local/lib/php/extensions/ -name xdebug.so)" > /usr/local/etc/php/conf.d/xdebug.ini \
    && echo "xdebug.remote_enable=on" >> /usr/local/etc/php/conf.d/xdebug.ini \
    && echo "xdebug.remote_autostart=off" >> /usr/local/etc/php/conf.d/xdebug.ini

RUN docker-php-ext-install sockets
