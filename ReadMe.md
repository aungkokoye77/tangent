## [Instruction]

tangent is project root folder.

src is application root folder.


### [How To Start]

1. (Host Machine) Go to project root folder $ cd tangent

2. (Host Machine) Change .env-example file accordingly and change name as 
$ mv .env-example .ev

3. (Host Machine) Start Docker $ docker-compose up -d

4. Go to tangent docker container $ docker exec -it tangent bash

5. (Docker Container) Run initial Soap API call for save in cache  $ php ./console/cache.php 

6. (Run RabbitMQ listener) $  php ./console/rabbit.php 

7. (RabbitMQ Control Panel Url)  http://localhost:15672

8. (Web App) http://localhost:8500

9. (Docker Container) For PHP Unit Testing, $ ./vendor/bin/phpunit --coverage-html coverage


#### [Docker]

* $cd tangent;

* $docker-compose up --build (run this command where docker-compose.yml exist)

#### [Composer]

Need to install composer

* $ cd ./src

* $composer install (run this command where composer.json exist)

#### [PHP Unit Test]

All test live in src/test folder

* $ cd ./src

* $ ./vendor/bin/phpunit --coverage-html coverage (run this command where phpunit.xml exist).


#### [PHP Unit Test coverage]

* Coverage html file  ./src/coverage

* Url: localhost:8500/coverage/