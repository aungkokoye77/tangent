<?php

namespace Test;

use App\IpStack;
use Exception;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Component\HttpFoundation\Request;

class IpStackTest extends TestCase
{

    /**
     * @dataProvider countryCodeDataProvider
     *
     * @param $ip
     * @param $expect
     * @param $key
     * @throws Exception
     */
    public function testGetCountryCode($ip, $expect, $key)
    {

        $request = Request::createFromGlobals();
        $request->server->set($key, $ip);
        $ipStack = new IpStack($request, new FilesystemAdapter());
        $this->assertEquals($expect, $ipStack->getCountryCode());

    }

    /**
     * @throws Exception
     */
    public function testException()
    {

        $request = Request::createFromGlobals();
        $request->server->set('REMOTE_ADDR', null);
        $request->server->set('HTTP_CLIENT_IP', null);
        $request->server->set('HTTP_X_FORWARDED_FOR', null);
        $ipStack = new IpStack($request, new FilesystemAdapter());
        $this->expectException(\Exception::class);
        $ipStack->getCountryCode();

    }

    /**
     * @return array
     */
    public function countryCodeDataProvider()
    {

        return [
            ["81.98.195.55", "GB", 'HTTP_X_FORWARDED_FOR'],
            ["213.143.46.18", "ES", 'HTTP_CLIENT_IP'],
            ["89.171.123.211", "PL", 'REMOTE_ADDR'],
            ["172.23.0.1", null, 'REMOTE_ADDR']
        ];

    }

}