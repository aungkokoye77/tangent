<?php


namespace Test;


use App\IpStack;
use App\SoapPopulation;
use Exception;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Component\HttpFoundation\Request;

class SoapPopulationTest extends TestCase
{
    /**
     * @dataProvider countryCodeDataProvider
     *
     * @param $ip
     * @param $country
     * @param $key
     * @param $capital
     * @throws Exception
     */
    public function testGetPopulation($ip, $country, $key, $capital)
    {
        $SoapPopulation = $this->getSoap($ip, $key);
        $resultArray    = $SoapPopulation->getPopulation();

        $this->assertEquals($country, $resultArray['country']['name']);
        $this->assertEquals($capital, $resultArray['country']['capital']);
        $this->assertArrayHasKey('population', $resultArray['country']);
    }

    /**
     * @dataProvider exceptionDataProvider
     *
     * @param $ip
     * @param $key
     * @throws \SoapFault
     * @throws Exception
     */
    public function testException($ip, $key)
    {
        $this->expectException(\Exception::class);
        $SoapPopulation = $this->getSoap($ip, $key);
        $SoapPopulation->getPopulation();
    }

    /**
     * @return array
     */
    public function countryCodeDataProvider()
    {

        return [
            ["81.98.195.55", "United Kingdom", 'HTTP_X_FORWARDED_FOR', 'London'],
            ["213.143.46.18", "Spain", 'HTTP_CLIENT_IP', 'Madrid'],
            ["89.171.123.211", "Poland", 'REMOTE_ADDR', 'Warsaw']
        ];

    }

    /**
     * @return array
     */
    public function exceptionDataProvider()
    {

        return [
            ["172.25.0.1", 'HTTP_X_FORWARDED_FOR'],
        ];

    }

    /**
     * @param $ip
     * @param $key
     * @return SoapPopulation
     * @throws Exception
     */
    private function getSoap($ip, $key)
    {
        $request = Request::createFromGlobals();
        $request->server->set($key, $ip);
        $ipStack = new IpStack($request, new FilesystemAdapter());
        return new SoapPopulation(new FilesystemAdapter(), $ipStack);
    }

}