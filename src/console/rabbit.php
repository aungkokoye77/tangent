<?php

require_once __DIR__ . '/../vendor/autoload.php';

use App\RabbitMQListener;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;

$receiver = new RabbitMQListener(new FilesystemAdapter());

try {
    $receiver->listen();
} catch (ErrorException $e) {
    echo "RabbitMQ Error:" . $e->getMessage();
} catch (Exception $e) {
    echo "Error:" . $e->getMessage();
}
