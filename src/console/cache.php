<?php

require_once __DIR__ . '/../vendor/autoload.php';

use App\RabbitMQSender;
use App\SoapPopulation;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;

$cache       = new FilesystemAdapter();
$client      = new SoapClient("http://ec2-3-8-36-112.eu-west-2.compute.amazonaws.com/ws/countries.wsdl");
$countryList = SoapPopulation::COUNTRY_LIST;
$errors      = [];

foreach ($countryList as $countryCode) {

    try {

        //send message to queue
        RabbitMQSender::sendMessage($countryCode);

    } catch (Exception $e) {

        $errors[] = "Error For -  {$countryCode} \n";

    }

}

if (!empty($errors)) {

    foreach ($errors as $error) {
        echo $error;
    }

} else {

    echo "All messages have been sent! \n";

}







