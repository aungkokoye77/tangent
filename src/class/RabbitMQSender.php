<?php


namespace App;

use Exception;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class RabbitMQSender
{
    protected const           HOST       = 'rabbit1';
    protected const           PORT       = 5672;
    protected const           USER       = 'guest';
    protected const           PASS       = 'guest';
    protected const           QUEUE_NAME = 'tangent';

    /**
     * @param $message
     * @throws Exception
     */
    public static function sendMessage($message)
    {

        $connection = new AMQPStreamConnection(
            self::HOST, self::PORT, self::USER, self::PASS
        );
        $channel    = $connection->channel();

        $channel->queue_declare(
            self::QUEUE_NAME, false, false, false, false
        );

        $msg = new AMQPMessage($message);
        $channel->basic_publish($msg, '', self::QUEUE_NAME);

        $channel->close();
        $connection->close();

    }

}