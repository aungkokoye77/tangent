<?php


namespace App;


interface iCountryCode
{
    public function getCountryCode();
}
