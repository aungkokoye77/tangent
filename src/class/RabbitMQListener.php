<?php


namespace App;


use DateTime;
use ErrorException;
use Exception;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use SoapClient;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;

class RabbitMQListener extends RabbitMQSender
{

    private $cache;

    /**
     * RabbitMQListener constructor.
     * @param FilesystemAdapter $cache
     */
    public function __construct(FilesystemAdapter $cache)
    {
        $this->cache = $cache;
    }

    /**
     *
     * @throws ErrorException
     * @throws Exception
     */
    public function listen()
    {

        $connection = new AMQPStreamConnection(
            self::HOST, self::PORT, self::USER, self::PASS
        );
        $channel    = $connection->channel();

        $channel->queue_declare(
            self::QUEUE_NAME, false, false, false, false
        );

        $callback = function ($msg) {

            $this->processApiCall($msg);

        };

        $channel->basic_consume(
            self::QUEUE_NAME,
            '',
            false, true, false, false, $callback);

        while ($channel->is_consuming()) {
            $channel->wait();
        }

        $channel->close();
        $connection->close();
    }

    /**
     * @param $msg
     * @return mixed
     * @throws Exception
     */
    public function processApiCall($msg)
    {
        $countryCode = $msg->body;

        try {
            $client      = new SoapClient(
                "http://ec2-3-8-36-112.eu-west-2.compute.amazonaws.com/ws/countries.wsdl"
            );
            $respond     = $client->getCountry(['code' => $countryCode]);
            $resultArray = json_decode(json_encode($respond), true);

            if (!empty($resultArray)) {

                $cacheMemory = $this->cache->getItem($countryCode);
                $cacheMemory->set($resultArray);
                $this->cache->save($cacheMemory);

                $dateTime = (new DateTime())->format('Y-m-d H:i:s');
                echo "Successfully save in cache -  {$countryCode} : {$dateTime} \n";

            }
        } catch (Exception $e) {

            echo "Rabbit Queue Listener Error: {$e->getMessage()} \n";

        }

    }

}