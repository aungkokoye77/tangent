<?php


namespace App;


use Exception;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Component\Cache\CacheItem;

/**
 * Countries API provide the details for the detected country according to country code.
 * http://ec2-3-8-36-112.eu-west-2.compute.amazonaws.com/ws/countries.wsdl
 *
 * Class SoapPopulation
 * @package App
 */
class SoapPopulation
{
    const COUNTRY_LIST = ['UK', 'ES', 'PL'];

    private $apiProvider;
    private $cache;
    private $countryCode;

    /**
     * SoapPopulation constructor.
     * @param FilesystemAdapter $cache
     * @param iCountryCode      $apiProvider
     * @throws Exception
     */
    public function __construct(FilesystemAdapter $cache, iCountryCode $apiProvider)
    {
        $this->apiProvider = $apiProvider;
        $this->cache       = $cache;
        $this->countryCode = $apiProvider->getCountryCode();

        if (!$this->validateCountryArray()) {
            throw new Exception("Country Code not valid: Cannot process the request!");
        }
    }

    /**
     * @return bool
     */
    private function validateCountryArray()
    {

        if (empty($this->countryCode)) {
            return false;
        }

        // Need to change GB to UK for soap api
        $this->countryCode = $this->countryCode === 'GB' ? 'UK' : $this->countryCode;

        if (!in_array($this->countryCode, self::COUNTRY_LIST)) {
            return false;
        }

        return true;

    }

    /**
     * @return mixed
     * @throws Exception
     */
    public function getPopulation()
    {
        //send message to queue
        RabbitMQSender::sendMessage($this->countryCode);

        //Find Item in catch by key
        /** @var CacheItem $cacheMemory */
        $cacheMemory = $this->cache->getItem($this->countryCode);
        return $cacheMemory->get();

    }

}