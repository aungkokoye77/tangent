<?php


namespace App;


use Exception;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Component\Cache\CacheItem;
use Symfony\Component\HttpFoundation\Request;

/**
 * Ipstack API provides country information according to IP address.
 * free for 10000 call for one month.
 * https://ipstack.com
 *
 * Class IpStack
 * @package App
 */
class IpStack implements iCountryCode
{
    private const API_KEY = '94ad982a0a83158fcbe1dc9f5be8c683';
    private $request;
    private $cache;

    /**
     * IpStack constructor.
     *
     * @param Request           $request
     * @param FilesystemAdapter $cache
     */
    public function __construct(Request $request, FilesystemAdapter $cache)
    {

        $this->request = $request;
        $this->cache   = $cache;

    }

    /**
     * check in cache by IP
     * If not in cache, make api call and save in cache for future use.
     *
     * @return CacheItem
     * @throws Exception
     */
    public function getCountryCode()
    {
        //Find Item in catch by key

        /** @var CacheItem $cacheMemory */
        $cacheMemory = $this->cache->getItem($this->getIP());

        //Item not found in cache. So, make api call and save in cache
        if (!$cacheMemory->isHit()) {

            $countryObj = $this->getResult();

            if (isset($countryObj['country_code'])) {

                $cacheMemory->set($countryObj['country_code']);

            } else {

                $cacheMemory->set(null);

            }

            $this->cache->save($cacheMemory);
            $cacheMemory->expiresAfter(3600);

        }

        return $cacheMemory->get();

    }

    /**
     * Get IP address from $_SERVER variables
     *
     * @return mixed|null
     * @throws Exception
     */
    public function getIP()
    {

        $server = $this->request->server;

        switch (true) {

            case !empty($server->get('HTTP_CLIENT_IP')) :
                $ip = $server->get('HTTP_CLIENT_IP');
                break;

            case !empty($server->get('HTTP_X_FORWARDED_FOR')) :
                $ip = $server->get('HTTP_X_FORWARDED_FOR');
                break;

            case !empty($server->get('REMOTE_ADDR')) :
                $ip = $server->get('REMOTE_ADDR');
                break;

            default :
                $ip = null;

        }

        if (!$ip) {
            throw new Exception("No IP Address: Cannot process the request!");
        }

        //$ip = "81.98.195.55";  //UK IP
        //$ip = "213.143.46.18"; //Spain IP
        //$ip = "89.171.123.211"; //Poland IP

        return $ip;

    }

    /**
     * make API call
     *
     * @return mixed
     * @throws Exception
     */
    private function getResult()
    {

        $ip = $this->getIP();

        $obj = file_get_contents("http://api.ipstack.com/" . $ip . "?access_key=" . self::API_KEY);
        return json_decode($obj, true);

    }

}