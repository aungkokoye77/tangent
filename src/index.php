<?php

require_once './vendor/autoload.php';

use App\IpStack;
use App\SoapPopulation;
use App\TwigTemplate;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Component\HttpFoundation\Request;

$twig        = TwigTemplate::getTwig();
$error       = null;
$resultArray = [];

try {

    $ipStack     = new IpStack(Request::createFromGlobals(), new FilesystemAdapter());
    $resultArray = (new SoapPopulation(new FilesystemAdapter(), $ipStack))->getPopulation();

} catch (Exception $e) {

    $error = $e->getMessage();

}

echo $twig->render('index.html.twig', [
    'result' => $resultArray['country'],
    'error'  => $error,
    'ip'     => $ipStack->getIP()
]);
